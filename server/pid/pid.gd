extends Node
class_name PID

# From https://github.com/robertcorponoi/unity-pid-with-rigidbodies

# The gain of the proportional error. This defines how much weight the proportional
# error has in the final output.
export var Kp: float = 1.0
# The gain of the integral error. This defines how much weight the integral
# error has in the final output.
export var Ki: float = 0.0
# The gain of the derivative error. This defines how much weight the derivative
# error has in the final output.
export var Kd: float = 0.1


# Tracks the current values of the proportional, integral, and derative errors.
var P: float = 0.0
var I: float = 0.0
var D: float = 0.0
# Used to keep track of what the error value was the last time the output was requested.
# This is used by the derivative to calculate the rate of change.
var previous_error: float = 0.0

# Returns The output of the PID calculation.
# current_error How far away the body is from the target rotation angle.
# dt is delta time from the update loop
func get_output(current_error: float, dt: float):
    # First we set the proportional to how how far we are from the target value.
    P = current_error

    # Since the integral is the sum of the proportional error over time we have
    # to multiply the propertional by the delta time and then add it on to the
    # current I value.
    I += P * dt
    
    # Set the derative to the rate of change of error by subtracting the current
    # error from the previous error to get the difference and then dividing it by
    # the amount of time it took to get to this change.
    D = (P - previous_error) / dt

    # Now we set the previous error to the current error to prepare for the next
    # time the output is requested.
    previous_error = current_error
    
    # Finally calculate the output using the values above multiplied by their gain.
    return P*Kp + I*Ki + D*Kd
