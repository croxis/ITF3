extends Spatial

var targets = []
var angular_velocity_break_active = true
var ai_pilot_active = false


# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.


func _physics_process(dt):
    return
    # set the initial value of the targetAngle to the object's. We do this so 
    # that we can easily make adjustments by just adding or subtracting from it.
    var target_angle = get_parent().rotation_degrees
    var angular_velocity_correction: Vector3 = Vector3(0,0,0)
    
    # Currently the targetAngle contains the current rotation of the ship.
    # Now we need to set the targetAngle to a combination of the user's 
    # input, the turn speed, and fixed delta time.
    target_angle += get_parent().thruster_torque * dt;


    # TODO: Bot angular-velocity navigation code will go here
    # target_angle += bot_thruster_torque * dt;
    
    # The angle controller drives the ship's angle towards the target angle.
    # This PID controller takes in the error between the ship's current rotation angle
    # and the target rotation angle as input, and returns a torque magnitude.
    # We use `DeltaAngle` to find the shortest difference between the two angles in degrees
    # which is what we need to pass to the PID controller.
    var angle_error = get_parent().rotation_degrees - target_angle
    var torque_correction_for_angle_x = $angle_controller.get_output(angle_error.x, dt);
    var torque_correction_for_angle_y = $angle_controller.get_output(angle_error.y, dt);
    var torque_correction_for_angle_z = $angle_controller.get_output(angle_error.z, dt);
    
    var torque_correction_for_angle: Vector3 = Vector3(torque_correction_for_angle_x, torque_correction_for_angle_y, torque_correction_for_angle_z)
    
    
    # The angular velocity controller drives the ship's angular velocity to 0.
    # This PID controller takes in the negated angular velocity of the ship and returns
    # a torque magnitude.
#    if angular_velocity_break_active:
#        if not get_parent().rotation_throttle.x:
#            var angular_velocity_error_x = -get_parent().angular_velocity.x
#            angular_velocity_correction.x = $angular_velocity_controller.get_output(angular_velocity_error_x, dt)
#        if not get_parent().rotation_throttle.y:
#            var angular_velocity_error_y = -get_parent().angular_velocity.y
#            angular_velocity_correction.y = $angular_velocity_controller.get_output(angular_velocity_error_y, dt)
#        if not get_parent().rotation_throttle.z:
#            var angular_velocity_error_z = -get_parent().angular_velocity.z
#            angular_velocity_correction.z = $angular_velocity_controller.get_output(angular_velocity_error_z, dt)

    var angular_velocity_error_x = -get_parent().angular_velocity.x
    angular_velocity_correction.x = $angular_velocity_controller.get_output(angular_velocity_error_x, dt)

    var angular_velocity_error_y = -get_parent().angular_velocity.y
    angular_velocity_correction.y = $angular_velocity_controller.get_output(angular_velocity_error_y, dt)

    var angular_velocity_error_z = -get_parent().angular_velocity.z
    angular_velocity_correction.z = $angular_velocity_controller.get_output(angular_velocity_error_z, dt)
    

    
    # The total torque from both controller is now applied to the ship. If the gains
    # are set correctly then the ship should rotate to the correct angle and stay there.
    var torque = torque_correction_for_angle + angular_velocity_correction * 10000  # Not sure why the pid controller isn't outputting enough
    if angle_error or torque:
        print("Results: ", angle_error, ", ", angular_velocity_correction, ", ", torque)
    get_parent().add_torque(get_parent().global_transform.basis * torque)
