extends Node2D


func _ready() -> void:
    name = 'main_server'
    # Connect functions to the events given by the networking system
    # warning-ignore:return_value_discarded
    loader.network.connect("server_created", self, "_on_server_created")
    # warning-ignore:return_value_discarded
    loader.network.connect("server_creation_failed", self, "_on_server_creation_failed")


func _on_server_created() -> void:
    # warning-ignore:return_value_discarded
    get_tree().change_scene("res://server/game_server.tscn")
    print("Server created")


func _on_server_creation_failed() -> void:
    $ui/err_dialog.popup_centered()


func _on_bt_start_pressed():
    # This is the default value, which will be used in case the specified one in the line edit is invalid
    var port: int = 2258
    if (!$ui/panel/GridContainer/txt_port.text.empty() && $ui/panel/GridContainer/txt_port.text.is_valid_integer()):
        port = $ui/panel/GridContainer/txt_port.text.to_int()
    
    # Same thing for maximum amount of players
    var mplayers: int = 6
    if (!$ui/panel/GridContainer/txt_maxplayers.text.empty() && $ui/panel/GridContainer/txt_maxplayers.text.is_valid_integer()):
        mplayers = $ui/panel/GridContainer/txt_maxplayers.text.to_int()
    
    # Try to create the server
    if (loader.is_dedicated_server):
        loader.network.set_dedicated_server_mode(true)
    loader.network.create_server(port, "Into the Fire", mplayers)
    print("Server spinning")
