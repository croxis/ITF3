extends Node

# Preaload the UI Player scene, which will be instantiated whenever a player joins the game
const uiplayer_scene: PackedScene = preload("res://server/ui/ui_player.tscn")

# Key = player ID
# Value = whatever data we need associated with the player.
var player_data: Dictionary = {}


func _ready() -> void:
    name = 'game_server'
    # warning-ignore:return_value_discarded
    loader.network.connect("player_added", self, "_on_player_added")
    # warning-ignore:return_value_discarded
    loader.network.connect("player_removed", self, "_on_player_removed")
    
    var bot_scene: PackedScene = preload("res://server/bot_pilot.tscn")
    # Spawn one hyperion
    var cnode: RigidBody = loader.network.snapshot_data.spawn_node(SnapShip, 100, hashcache.hyperion)
    # Place the character at the spawn point
    cnode.global_transform.origin = Vector3(5690.76, 10, -575)
    cnode.rotation_degrees = Vector3(90, 0, 0)
    
    # Spawn one omega
    var onode: RigidBody = loader.network.snapshot_data.spawn_node(SnapShip, 101, hashcache.omega)
    # Place the character at the spawn point
    onode.global_transform.origin = Vector3(6090.76, 10, 475)
    onode.rotation_degrees = Vector3(-45, 0, 0)
    
    # Spawn one AI Fury
    var starnode: RigidBody = loader.network.snapshot_data.spawn_node(SnapShip, 102, hashcache.star_fury)
    # Place the character at the spawn point
    starnode.global_transform.origin = Vector3(5090.76, 10, 1000)
    starnode.rotation_degrees = Vector3(-45, 0, 0)
    
    var bot: Spatial = bot_scene.instance()
    starnode.add_child(bot)
    


func _on_bt_quit_pressed() -> void:
    # Close the server
    loader.network.close_server()
    # Then quit the app
    get_tree().quit()


func _on_bt_close_pressed() -> void:
    # Close the server
    loader.network.close_server()
    # Return to the main menu
    # warning-ignore:return_value_discarded
    get_tree().change_scene("res://server/main_server.tscn")


func _on_player_added(pid: int) -> void:
    # Create instance of the UI element
    var uielement: Node = uiplayer_scene.instance()
    uielement.set_data(pid)
    
    # Attach it into the vbox
    $ui/scroll/vbox.add_child(uielement)
    
    # Cache the node
    player_data[pid] = {
        "ui_element": uielement,
    }
    
    # Spawn the character node
    var cnode: RigidBody = loader.network.snapshot_data.spawn_node(SnapShip, pid, hashcache.star_fury)
    # Obtain the index of the spawn point
    var index: int = loader.network.player_data.get_player_count() - 1
    # Place the character at the spawn point
    cnode.global_transform.origin = $game.get_spawn_position(index)
    cnode.connect("destroyed", self, "respawn")
    var bot_scene: PackedScene = preload("res://server/bot_pilot.tscn")
    var bot: Spatial = bot_scene.instance()
    cnode.add_child(bot)


func _on_player_removed(pid: int) -> void:
    # Obtain the player data
    var pdata: Dictionary = player_data.get(pid, {})
    
    # Bail if the obtained entry does not exist (IE.: it's empty)
    if (pdata.empty()):
        return
    
    # "ui_element" should point to the node within the vbox container. So, queue free it
    pdata.ui_element.queue_free()
    # Then erase from the cached data
    # warning-ignore:return_value_discarded
    player_data.erase(pid)
    
    # De-spawn the player character
    loader.network.snapshot_data.despawn_node(SnapShip, pid)


func _physics_process(_dt: float) -> void:
    # Request initialization of the snapshot for this physics frame
    loader.network.init_snapshot()


func _exit_tree() -> void:
    loader.network.reset_system()


func respawn(pid) -> void:
    # De-spawn the player character
    loader.network.snapshot_data.despawn_node(SnapShip, pid)
    # Spawn the character node
    var cnode: RigidBody = loader.network.snapshot_data.spawn_node(SnapShip, pid, hashcache.star_fury)
    # Obtain the index of the spawn point
    var index: int = loader.network.player_data.get_player_count() - 1
    # Place the character at the spawn point
    cnode.global_transform.origin = $game.get_spawn_position(index)
    cnode.connect("destroyed", self, "respawn")
    var bot_scene: PackedScene = preload("res://server/bot_pilot.tscn")
    var bot: Spatial = bot_scene.instance()
    cnode.add_child(bot)
