extends Button
signal join_server(ip, port)

func _pressed():
    print("Pressed")
    var port: int = 2258
    
    if (!get_node("../GridContainer/txt_port").text.empty() && get_node("../GridContainer/txt_port").text.is_valid_integer()):
        port = get_node("../GridContainer/txt_port").text.to_int()
    
    var ip: String = get_node("../GridContainer/txt_ip").text
    if (ip.empty()):
        return
    print("Joining server", ip, port)
    loader.network.join_server(ip, port)
    print("Join sent")
    emit_signal("join_server", ip, port)
