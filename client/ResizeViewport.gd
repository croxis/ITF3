extends Viewport

export (float) var scale = 1


func _ready():
    get_tree().root.connect("size_changed", self, "_on_viewport_size_changed")

func _on_viewport_size_changed():
    # Do whatever you need to do when the window changes!
    size = OS.get_window_size() * scale
    #print("Resized to: ", size)
    
