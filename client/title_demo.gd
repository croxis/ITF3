extends Spatial

var in_cnc: bool = false

func _ready():
    # warning-ignore:return_value_discarded
    $Ships/babylon_5/AnimationPlayer/vr_player/ARVRController_Left.connect("button_pressed", self, "button_pressed")
    # warning-ignore:return_value_discarded
    $Ships/babylon_5/AnimationPlayer/vr_player/ARVRController_Right.connect("button_pressed", self, "button_pressed")
    $Ships/babylon_5/AnimationPlayer/vr_player.hide_controllers()
    

func button_pressed(button_index):
    if button_index == JOY_VR_TRIGGER and not in_cnc:
        vr_moveto_cnc()
    if button_index == JOY_VR_TRIGGER and in_cnc and loader.ip and loader.port:
        print("Joining server", loader.ip, loader.port)
        loader.network.join_server(loader.ip, loader.port)
        emit_signal("join_server", loader.ip, loader.port)

func vr_moveto_cnc():
    in_cnc = true
    ARVRServer.center_on_hmd(2, true)
    $Ships/babylon_5/AnimationPlayer.stop(false)
    var vr_player = $Ships/babylon_5/AnimationPlayer/vr_player
    $Ships/babylon_5/AnimationPlayer/vr_player.show_controllers()
    var old_parent = vr_player.get_parent()
    old_parent.remove_child(vr_player)
    $Ships/babylon_5/rotation/CnC.add_child(vr_player)
    vr_player.translation = Vector3(0.007, 0, 0)
    vr_player.scale = Vector3(1/$Ships/babylon_5.scale.x, 1/$Ships/babylon_5.scale.x, 1/$Ships/babylon_5.scale.x)
