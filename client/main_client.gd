extends Spatial

func _ready() -> void:
    # warning-ignore:return_value_discarded
    loader.network.connect("join_fail", self, "_on_join_failed")
    # warning-ignore:return_value_discarded
    loader.network.connect("join_accepted", self, "_on_join_accepted")
    
    var vr
    #vr = ARVRServer.find_interface("OpenVR")
    #vr = ARVRServer.find_interface("OpenXR")
    if ("--openvr" in OS.get_cmdline_args()):
        print("Finding OpenVR")
        vr = ARVRServer.find_interface("OpenVR")
        print("Found OpenVR")
    if ("--openxr" in OS.get_cmdline_args()):
        print("Finding OpenXR")
        vr = ARVRServer.find_interface("OpenXR")
 
    if vr and vr.initialize():
        print("Setting up VR")
        loader.xr = true
        get_viewport().arvr = true
        get_viewport().keep_3d_linear = true
        #OS.set_window_maximized(true)
        print("Size: ", get_viewport().size)
        print(OS.window_size)
        OS.vsync_enabled = false
        Engine.target_fps = 90
        Engine.iterations_per_second= 90
        $title_demo/Celestial/ran_system.scene_camera = $title_demo/Ships/babylon_5/AnimationPlayer/vr_player
        ProjectSettings.set_setting("rendering/quality/directional_shadow/size", 4096)
        ProjectSettings.set_setting("rendering/quality/shadow_atlas/cubemap_size", 512)
        ProjectSettings.set_setting("rendering/quality/shadow_atlas/size", 4096)
        print("VR setup")
    print("Menu Setup Complete")
    
    $load_screen.load_scene("res://client/title_demo.tscn")


func _on_join_failed() -> void:
    $ui/err_dialog.popup_centered()
    print("join failed")


func _on_join_accepted() -> void:
    print("join accepting")
    # warning-ignore:return_value_discarded
    get_tree().change_scene("res://client/game_client.tscn")
    print("join accepted")
