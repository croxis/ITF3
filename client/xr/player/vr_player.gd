extends ARVROrigin

func hide_controllers():
    $ARVRController_Left/LeftHand.visible = false
    $ARVRController_Right/RightHand.visible = false
    $ARVRController_Right/Function_pointer.show_laser = false
    $ARVRController_Right/Function_pointer.show_target = false
    
func show_controllers():
    $ARVRController_Left/LeftHand.visible = true
    $ARVRController_Right/RightHand.visible = true
    $ARVRController_Right/Function_pointer.show_laser = true
    $ARVRController_Right/Function_pointer.show_target = true
