tool
extends RigidBody
class_name Ship


# Cache the entity unique ID in here
var _uid: int = 0
# Cache incoming server state. It includes a flag telling if the data is actually a correction or not
var _correction: Dictionary = { "has_correction": false }
var _debug_all_stop: bool = false

# Thrust values. Eventually this will be per engine
# Max thrust is 8 gs
# Thrust = 3800000 N
# Array +x, -x, +y, -y, +z, -z
# In furute individual engines will be modeled. This is the "aggrigate" of all
# engines in that direction
export var max_thrust = [3800000, 3800000 / 2, 3800000 / 4, 3800000 / 4, 3800000 / 4, 3800000 / 4]

# Eventually each individual engine will be modeled to react to the desired
# pilot input. For now it is just generic
# For now throttle is capped at  1. In the future we can boost power to engines.
# In Godot 3.2 add_force is cleared every physics frame
var throttle: Vector3 = Vector3(0.0, 0.0,  0.0)
var rotation_throttle: Vector3 = Vector3(0.0, 0.0,  0.0)
# Engine distance from origin. Estimated by eyeballing. For torque
var engine_length = 7.5
var thruster_force: Vector3 = Vector3(0, 0, 0)
var thruster_torque: Vector3 = Vector3(0, 0, 0)

var health = 100
var start_speed = 0

signal destroyed(_uid)


# Called when the node enters the scene tree for the first time.
func _ready():
    # If in editor simply disable processing as it's not needed here
    if (Engine.is_editor_hint()):
        set_physics_process(false)
    
    # Retrieve the entity unique ID, caching it for easier usage later
    if (has_meta("uid")):
        _uid = get_meta("uid")
    
    if (loader.network.is_id_local(_uid)):
        # Create the camera
        var cam: Camera = Camera.new()
        # Attach to the node hierarchy
        $NetMeshInstancePod/camera_pos.add_child(cam)
        cam.set_znear(0.3)
        cam.set_zfar(20000)
        # Ensure it is the active one
        cam.current = true
    
    can_sleep = false


func _integrate_forces(state: PhysicsDirectBodyState):
    # Check if there is any correction to be done
    # Warning, in the tutorial this was in _physics_process
    if (_correction.has_correction):
        # Yes, there is. Apply it
        state.transform.origin = _correction.position
        state.transform.basis = Basis(_correction.orientation)
        throttle = _correction.throttle
        rotation_throttle = _correction.rotation_throttle
        state.angular_velocity = _correction.angular_velocity
        state.linear_velocity = _correction.linear_velocity
        set_health(_correction.health)
        # Reset the flag so correction doesn't occur when not needed
        _correction.has_correction = false
        
        # Replay input objects within internal history if this character belongs to local player
        if (loader.network.is_id_local(_uid)):
            var input_list: Array = loader.network.player_data.local_player.get_cached_input_list()
            for i in input_list:
            #    _handle_input(state.dt, i)
                _handle_input(i)
                loader.network.correct_in_snapshot(generate_snap_object(), i)
    if _debug_all_stop:
        state.angular_velocity = Vector3(0,0,0)
        state.linear_velocity = Vector3(0,0,0)
        _debug_all_stop = false
    $NetMeshInstancePod/Viewport_HUD/Control.set_speed(state.linear_velocity.length())


func _physics_process(dt: float) -> void:
    #_handle_input(dt, loader.network.get_input(_uid))
    _handle_input(loader.network.get_input(_uid))
    
    if throttle.x > 0:
        thruster_force.x = max_thrust[4] * throttle.x
    elif throttle.x < 0:
        thruster_force.x = max_thrust[5] * throttle.x
    else:
        thruster_force.x = 0
    if throttle.y > 0:
        thruster_force.y = max_thrust[2] * throttle.y
    elif throttle.y < 0:
        thruster_force.y = max_thrust[3] * throttle.y
    else:
        thruster_force.y = 0
    if throttle.z > 0:
        thruster_force.z = max_thrust[0] * throttle.z
    elif throttle.z < 0:
        thruster_force.z = max_thrust[1] * throttle.z
    else:
        thruster_force.z = 0
    # add_force is cleared after every frame
    add_central_force(global_transform.basis * thruster_force)
    
    # Torque is, simplified, force times length. The length of the engines from
    # axis is currently eyeballed. Math will be more elaborate later when indiv
    # thrusters are moddled
    
    if rotation_throttle.x > 0:
        thruster_torque.x = engine_length * max_thrust[5] 
    elif rotation_throttle.x < 0:
        thruster_torque.x = -engine_length * max_thrust[5]
    else:
        thruster_torque.x = 0
    if rotation_throttle.y > 0:
        thruster_torque.y = engine_length * max_thrust[5]
    elif rotation_throttle.y < 0:
        thruster_torque.y = -engine_length * max_thrust[5]
    else:
        thruster_torque.y = 0
    if rotation_throttle.z > 0:
        thruster_torque.z = engine_length * max_thrust[5]
    elif rotation_throttle.z < 0:
        thruster_torque.z = -engine_length * max_thrust[5]
    else:
        thruster_torque.z = 0
    thruster_torque = thruster_torque / 7  # Modifyer to make the fury not SOOOO twitchy
    add_torque(global_transform.basis * thruster_torque)
    
    
    loader.network.snapshot_entity(generate_snap_object())
    $NetMeshInstancePod/Viewport_HUD/Control.set_acceleration((linear_velocity.length() - start_speed)/dt)
    $NetMeshInstancePod/UIScreens/LeftWindowView/Velocity.current_velocity = int(global_transform.basis.xform_inv(linear_velocity).z)
    $NetMeshInstancePod/UIScreens/MiddleWindowView/X.current_velocity = -int(global_transform.basis.xform_inv(linear_velocity).x)
    $NetMeshInstancePod/UIScreens/RightWindowView/Y.current_velocity = int(global_transform.basis.xform_inv(linear_velocity).y)
    start_speed = linear_velocity.length()


# InputData is a class type defined within the Network addon.
#func _handle_input(dt: float, input: InputData) -> void:
func _handle_input(input: InputData) -> void:
    throttle = Vector3(0, 0, 0)
    rotation_throttle = Vector3(0, 0, 0)
    if (!input):
        return
    
    # Analog input is squared so small joy movements are more precice 
    if (input.get_analog("throttle_forward")):
       throttle.z = pow(input.get_analog("throttle_forward"), 3)
    if(input.get_analog("throttle_backward")):
       throttle.z = -pow(input.get_analog("throttle_backward") , 3)
    if (input.get_analog("left_rotation")):
       rotation_throttle.y = pow(input.get_analog("left_rotation"), 6)
    if(input.get_analog("right_rotation")):
       rotation_throttle.y = -pow(input.get_analog("right_rotation"), 6) 
    if (input.get_analog("down_rotation")):
       rotation_throttle.x = pow(input.get_analog("down_rotation"), 6)
    if(input.get_analog("up_rotation")):
       rotation_throttle.x = -pow(input.get_analog("up_rotation"), 6)
    if (input.get_analog("clock_rotation")):
       rotation_throttle.z = -pow(input.get_analog("clock_rotation"), 6)
    if(input.get_analog("counter_rotation")):
       rotation_throttle.z = pow(input.get_analog("counter_rotation"), 6)
    
    if (input.is_pressed("forward_full_thrust")):
        throttle.z = 1
    if (input.is_pressed("backward_full_thrust")):
        throttle.z = -1
    if (input.is_pressed("slide_left_full")):
        throttle.x = 1
    if (input.is_pressed("slide_right_full")):
        throttle.x = -1
    if (input.is_pressed("slide_up_full")):
        throttle.y = 1
    if (input.is_pressed("slide_down_full")):
        throttle.y = -1
    if (input.is_pressed("rotate_left_full")):
        rotation_throttle.y = 1
    if (input.is_pressed("rotate_right_full")):
        rotation_throttle.y = -1
    if (input.is_pressed("rotate_down_full")):
        rotation_throttle.x = 1
    if (input.is_pressed("rotate_up_full")):
        rotation_throttle.x = -1
    if (input.is_pressed("clockwise_full")):
        rotation_throttle.z = 1
    if (input.is_pressed("counter_clockwise_full")):
        rotation_throttle.z = -1
    if (input.is_pressed("debug_all_stop")):
        throttle = Vector3(0, 0, 0)
        rotation_throttle = Vector3(0, 0, 0)
        _debug_all_stop = true


func apply_state(state: Dictionary) -> void:
    # Take the server state and cache it
    _correction["position"] = state.position
    _correction["orientation"] = state.orientation
    _correction["throttle"] = state.throttle
    _correction["rotation_throttle"] = state.rotation_throttle
    _correction["angular_velocity"] = state.angular_velocity
    _correction["linear_velocity"] = state.linear_velocity
    _correction["health"] = state.health
    # And set the flag indicating that a correction is necessary
    _correction["has_correction"] = true


func generate_snap_object() -> SnapShip:
    # Second argument, class hash, is required but we don't use it, so setting to 0
    var snap: SnapShip = SnapShip.new(_uid, get_meta("chash"))
    
    # Transfer the character state into the object
    #snap.position = global_transform.origin
    #snap.orientation = global_transform.basis.get_rotation_quat()
    snap.position = transform.origin
    snap.orientation = transform.basis.get_rotation_quat()
    snap.throttle = throttle
    snap.rotation_throttle = rotation_throttle
    snap.angular_velocity = angular_velocity
    snap.linear_velocity = linear_velocity
    snap.health = health
    
    return snap


func bullet_hit(damage, bullet_global_trans):
    print("Hit")
    var BASE_BULLET_BOOST = 1
    var direction_vect = bullet_global_trans.basis.z.normalized() * BASE_BULLET_BOOST
    apply_impulse((bullet_global_trans.origin - global_transform.origin).normalized(), direction_vect * damage)
    set_health(health - damage)


func set_health(new_health):
    health = new_health
    $NetMeshInstancePod/Viewport_HUD/Control.set_health(health)
    if (health <= 0):
        emit_signal("destroyed", _uid)
