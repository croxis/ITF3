extends Spatial

const DAMAGE = 15
const COOLDOWN = 1

var fire = false
var gun_cooldowns = [0, 0, 0, 0]
var guns = []


# InputData is a class type defined within the Network addon.
func _handle_input(_dt: float, input: InputData) -> void:
    if (!input):
        return
    if (!input.is_pressed("shoot_primary")):
        fire = false
        return
    if (input.is_pressed("shoot_primary")):
        fire = true


func _physics_process(dt: float) -> void:
    for i in range(0, gun_cooldowns.size()):
        if (gun_cooldowns[i] > 0):
            gun_cooldowns[i] -= dt
        if (gun_cooldowns[i] < 0):
            gun_cooldowns[i] = 0
    
    _handle_input(dt, loader.network.get_input(get_parent()._uid))
    
    # Make sure this is the last part of the physics function because of the return!
    if (fire and loader.is_dedicated_server):
        # Pew, fire only one gun. For now
        for i in range(0, gun_cooldowns.size()):
            if (gun_cooldowns[i] == 0):
                # Pew!
                gun_cooldowns[i] = COOLDOWN
                guns[i].fire(DAMAGE)
                return


func _ready() -> void:
    guns = [$Bolt0, $Bolt1, $Bolt2, $Bolt3]
