extends Spatial
# Code for a single gun


func fire(damage):
    $AudioStreamPlayer3D.play()
    if (loader.is_dedicated_server):
        var bolt_node: Bolt = loader.network.snapshot_data.spawn_node(SnapBolt, hashcache.get_bolt_id(), hashcache.bolt)
        bolt_node.global_transform.origin = global_transform.origin
        bolt_node.global_transform.basis = global_transform.basis
        bolt_node.set_speed(get_parent().get_parent().linear_velocity.length())
