extends Spatial

export(NodePath) var scene_camera_path
var scene_camera

#func _ready():
#    var scene_camera = get_node(scene_camera_path)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
    scene_camera = get_viewport().get_camera()
    if scene_camera:
        global_transform.origin.x = scene_camera.global_transform.origin.x
        global_transform.origin.y = scene_camera.global_transform.origin.y
        global_transform.origin.z = scene_camera.global_transform.origin.z
