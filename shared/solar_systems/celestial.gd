tool
extends Spatial
class_name Celestial

export (NodePath) var scene_camera

# This is a generic celestial item. Can work for planets, stars, stations
# We're doing simple orbits for some cases, like B5 and moons.
# Planets will rotate for a day/night cycle, but will otherwise be stationary
# I may change my mind if hyperspace transit time changes significantly for
# in system motion
# All time will be in seconds, all distances in meters

export (float) var distance_to_parent_au = 0 setget set_distance_au
export (float) var distance_to_parent_km = 0 setget set_distance_km
export (float) var orbital_period_e_years = 0 setget set_period_years
export (float) var orbital_period_e_days = 0 setget set_period_days
export (float) var rotation_e_days = 0 setget set_rotation_time
export (float) var size_e_radii = 0 setget set_e_radius
export (float) var size_sun_radii = 0 setget set_sun_radius

var distance_to_parent_m = 0
var orbital_period_s = 0
var orbital_angular_rate = 0  # Degrees per second
var current_angle = 0
var radius = 1
var rotation_s = 0

# Starting position in the orbit. Change to put objects in Lagrange points
export (float) var starting_angle = 0

# Do we orbit the parent or just look prerdy?
export (bool) var active_orbit = false

# Vector3 only stores as 32 bit floats, we need the 64 bit float
# These will be calculated on clients.
var location_x = 0
var location_y = 0
var location_z = 0

# Handy constants for math into meters
const EARTH_RADIUS = 6.3781 * pow(10, 6)
const SUN_RADIUS = 695700000
const AU = 1.495978707 * pow(10, 11)
# And time into seconds
const SECONDS_IN_EARTH_DAY = 86400
const SECONDS_IN_EARTH_YEAR = SECONDS_IN_EARTH_DAY * 365.25


func set_distance_au(new_distance):
    distance_to_parent_m = new_distance * AU
    distance_to_parent_km = new_distance * AU / 1000
    distance_to_parent_au = new_distance
    #print("DTP: ", name, " ", distance_to_parent_m)
    print(name, " ", distance_to_parent_m, " ", distance_to_parent_km, " ", distance_to_parent_au)
    property_list_changed_notify()
    _update_position()


func set_distance_km(new_distance):
    distance_to_parent_m = new_distance * 1000
    distance_to_parent_km = new_distance
    distance_to_parent_au = new_distance / AU / 1000
    #print("DTP: ", name, " ", distance_to_parent_m)
    property_list_changed_notify()
    _update_position()


func set_period_years(new_year):
    orbital_period_e_years = new_year
    orbital_period_e_days = new_year * 356.25
    orbital_period_s = new_year * SECONDS_IN_EARTH_YEAR
    if orbital_period_s:
        orbital_angular_rate = 360 / orbital_period_s
    property_list_changed_notify()
    _update_position()


func set_period_days(new_days):
    orbital_period_e_years = new_days/365.25
    orbital_period_e_days = new_days
    orbital_period_s = new_days * SECONDS_IN_EARTH_DAY
    if orbital_period_s:
        orbital_angular_rate = 360 / orbital_period_s
    property_list_changed_notify()
    _update_position()


func set_e_radius(new_radius):
    radius = new_radius * EARTH_RADIUS
    size_e_radii = new_radius
    size_sun_radii = new_radius * EARTH_RADIUS / SUN_RADIUS
    property_list_changed_notify()
    _update_position()


func set_sun_radius(new_radius):
    radius = new_radius * SUN_RADIUS
    size_e_radii = new_radius * SUN_RADIUS / EARTH_RADIUS
    size_sun_radii = new_radius
    property_list_changed_notify()
    _update_position()


func set_rotation_time(new_days):
    rotation_e_days = new_days
    rotation_s = new_days * SECONDS_IN_EARTH_DAY
    property_list_changed_notify()
    _update_position()


func _update_position():
    if get_parent():
        if "location_x" in get_parent():
            location_x = distance_to_parent_m * sin(current_angle) + get_parent().location_x
            location_z = distance_to_parent_m * -cos(current_angle) + get_parent().location_z


# Called when the node enters the scene tree for the first time.
func _ready():
    current_angle = starting_angle
    _update_position() 


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
    if not active_orbit:
        return
    current_angle += orbital_angular_rate * delta
    _update_position() #TODO: Update to pull time from game.time
