extends "res://shared/solar_systems/celestial.gd"
class_name Star

export (float) var temperature = 5700 setget k_to_rgb
export (float) var luminocity_sun = 1
var color = Color(0, 0, 0)


func calculate_luminocity():
    # Calculates luminocity based on celestial distance of the star.
    # https://en.wikipedia.org/wiki/Solar_constant
    # https://en.wikipedia.org/wiki/Solar_luminosity
    var luminocity = luminocity_sun * 3.838 *  pow(10, 26)
    var distance_in_meters = 150000000  # TODO: Actually calculate celestial distance from camera
    var watt = luminocity / (4 * PI * 1 * pow(distance_in_meters, 2))
    # At 1 Au the sun produces about 1.3 kW per m^2. Godot don't have physical
    # based lighting, so we will divide by a fundge number instead of 1000
    $DirectionalLight.light_energy = watt / 500
    

func k_to_rgb(new_temperature):
    # This code is from https://gist.github.com/petrklus/b1f427accdf7438606a6
    # based on the work of https://tannerhelland.com/2012/09/18/convert-temperature-rgb-algorithm-code.html
    # and http://www.vendian.org/mncharity/dir3/blackbody/
    # Uses updated equations from https://www.zombieprototypes.com/?p=210
    # Old equations commented out
    
    # range check
    if new_temperature < 0:
        new_temperature = 0
    temperature = new_temperature
    if temperature < 1000:
        color = Color8(0, 0, 0)
        return
    if temperature > 40000:
        temperature = 40000
    
    var tmp = temperature / 100.0
    
    # Red
    if tmp <= 66:
        color.r8 = 255
    else:
        #var tmp_red = 329.698727446 * pow(tmp - 60, -0.1332047592)
        var tmp_red = 351.97690566805693 + 0.114206453784165 * (tmp - 55) + -40.25366309332127 * log(tmp - 55)
        if tmp_red < 0:
            color.r = 0
        elif tmp_red > 255:
            color.r8 = tmp_red
        else:
            color.r8 = tmp_red

    # Green
    if tmp <= 66:
        #var tmp_green = 99.4708025861 * log(tmp) - 161.1195681661
        var tmp_green = -155.25485562709179 + -0.44596950469579133 * (tmp - 2) + 104.49216199393888 * log(tmp - 2)
        if tmp_green < 0:
            color.g = 0
        elif tmp_green > 255:
            color.g8 = 255
        else:
            color.g8 = tmp_green
    else:
        #var tmp_green = 288.1221695283 * pow(tmp - 60, -0.0755148492)
        var tmp_green = 325.4494125711974 + 0.07943456536662342 * (tmp - 50) + -28.0852963507957 * log(tmp - 50)
        if tmp_green < 0:
            color.g = 0
        elif tmp_green > 255:
            color.g8 = 255
        else:
            color.g8 = tmp_green
    
    # Blue
    if tmp >=66:
        color.b8 = 255
    elif tmp <= 20:
        color.b = 0
    else:
        #var tmp_blue = 138.5177312231 * log(tmp - 10) - 305.0447927307
        var tmp_blue = -254.76935184120902 + 0.8274096064007395 * (tmp - 10) + 115.67994401066147 * log(tmp - 10)
        if tmp_blue < 0:
            color.b = 0
        elif tmp_blue > 255:
            color.b8 = 255
        else:
            color.b8 = tmp_blue
    
    $DirectionalLight.light_color = color
    
    if $NetMeshInstance._mesh_node:
        $NetMeshInstance._mesh_node.get_surface_material(0).albedo_color = color
    print_debug("Color change: ", color)
