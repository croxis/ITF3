extends Area
class_name Bolt

var BULLET_DAMAGE = 10
var BULLET_SPEED = 100

const KILL_TIMER = 10
var timer = 0

var hit_something = false

# Cache the entity unique ID in here
var _uid: int = 0


func apply_state(state: Dictionary) -> void:
    # We are not ussing the _coorection dictoary for simple bolts
    # Homing missils will need it
    # Take the server state and cache it
    global_transform.origin = state.position
    global_transform.basis = Basis(state.orientation)
    timer = state.timer
    BULLET_SPEED = state.speed


func generate_snap_object() -> SnapBolt:
    # Second argument, class hash, is required but we don't use it, so setting to 0
    var snap: SnapBolt = SnapBolt.new(_uid, hashcache.bolt)
    
    # Transfer the character state into the object
    snap.position = global_transform.origin
    snap.orientation = global_transform.basis.get_rotation_quat()
    snap.timer = timer
    snap.speed = BULLET_SPEED
    
    return snap


func _ready():
    connect("body_entered", self, "collided")
    # Retrieve the entity unique ID, caching it for easier usage later
    if (has_meta("uid")):
        _uid = get_meta("uid")


func set_speed(speed: float):
    BULLET_SPEED = BULLET_SPEED + speed


func _physics_process(delta):
    var forward_dir = global_transform.basis.z.normalized()
    global_translate(forward_dir * BULLET_SPEED * delta)
    timer += delta
    #var n = 1 + 0.2 * timer
    #scale = Vector3(n, n, n)
    
    # Snapshot current state
    loader.network.snapshot_entity(generate_snap_object())
    
    if timer >= KILL_TIMER:
        despawn()


func collided(body):
    if hit_something == false:
        if body.has_method("bullet_hit"):
            body.bullet_hit(BULLET_DAMAGE/scale.x, global_transform)

    hit_something = true
    despawn()


func despawn():
    loader.network.snapshot_data.despawn_node(SnapBolt, _uid)
