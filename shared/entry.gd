extends Node


func _ready() -> void:
    name = 'entry'
    # By default open the client main menu
    var spath: String = "res://client/main_client.tscn"
    if ("--server" in OS.get_cmdline_args() || OS.has_feature("dedicated_server")):
        # Requesting to open in server mode, so change the string to it
        spath = "res://server/main_server.tscn"
        # Cache that we are indeed in server mode
        loader.is_dedicated_server = true
        print("Set to server mode")
    
    var arguments = {}
    for argument in OS.get_cmdline_args():
        # Parse valid command-line arguments into a dictionary
        if argument.find("=") > -1:
            var key_value = argument.split("=")
            arguments[key_value[0].lstrip("--")] = key_value[1]
    if "ip" in arguments and "port" in arguments:
        loader.ip = arguments["ip"]
        loader.port = int(arguments["port"])
    
    print("Args: ", OS.get_cmdline_args())
    
    # And transition into the appropriate scene
    # warning-ignore:return_value_discarded
    get_tree().change_scene(spath)
