extends Node


var bolt
var hyperion
var omega
var star_fury


# Weapon bolt ids are 10,000 to 50,000
var bolt_id: int = 10000


func get_bolt_id() -> int:
    bolt_id += 1
    if (bolt_id > 49999):
        bolt_id = 10000
    return bolt_id
