extends Node

func _ready() -> void:
    name = 'gamestate'
    # Register input within the network system
    loader.network.register_action("forward_full_thrust", false)
    loader.network.register_action("backward_full_thrust", false)
    loader.network.register_action("rotate_left_full", false)
    loader.network.register_action("rotate_right_full", false)
    loader.network.register_action("rotate_down_full", false)
    loader.network.register_action("rotate_up_full", false)
    loader.network.register_action("clockwise_full", false)
    loader.network.register_action("counter_clockwise_full", false)
    loader.network.register_action("debug_all_stop", false)
    loader.network.register_action("slide_left_full", false)
    loader.network.register_action("slide_right_full", false)
    loader.network.register_action("slide_up_full", false)
    loader.network.register_action("slide_down_full", false)
    
    # Joystick
    loader.network.register_action("throttle_forward", true)
    loader.network.register_action("throttle_backward", true)
    loader.network.register_action("left_rotation", true)
    loader.network.register_action("right_rotation", true)
    loader.network.register_action("up_rotation", true)
    loader.network.register_action("down_rotation", true)
    loader.network.register_action("clock_rotation", true)
    loader.network.register_action("counter_rotation", true)

    # Combat
    loader.network.register_action("shoot_primary", false)
