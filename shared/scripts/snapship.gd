extends SnapEntityBase
class_name SnapShip

var position: Vector3
var orientation: Quat
var throttle: Vector3
var rotation_throttle: Vector3
var angular_velocity: Vector3
var linear_velocity: Vector3
var health: int


func _init(id: int, chash: int = 0).(id, chash) -> void:
    position = Vector3()
    orientation = Quat()
    throttle = Vector3()
    rotation_throttle = Vector3()
    angular_velocity = Vector3()
    linear_velocity = Vector3()
    health = int()

func apply_state(n: Node) -> void:
    if (n is RigidBody && n.has_method("apply_state")):
        n.apply_state({
            "position": position,
            "orientation": orientation,
            "throttle": throttle,
            "rotation_throttle": rotation_throttle,
            "angular_velocity": angular_velocity,
            "linear_velocity": linear_velocity,
            "health": health
        })


