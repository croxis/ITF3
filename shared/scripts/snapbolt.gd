extends SnapEntityBase
class_name SnapBolt

var position: Vector3
var orientation: Quat
var speed: float
var timer: float
var global_basis: Vector3

func _init(id: int, chash: int = 0).(id, chash) -> void:
    position = Vector3()
    orientation = Quat()
    timer = float()
    speed = float()

func apply_state(n: Node) -> void:
    if (n is Area && n.has_method("apply_state")):
        n.apply_state({
            "position": position,
            "orientation": orientation,
            "timer": timer,
            "speed": speed,
        })


