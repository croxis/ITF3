extends SnapEntityBase
class_name SnapEnvironment

# Seconds past Jan 1 2258
var time: float = 0



func _init(uid: int, h: int).(uid, h) -> void:
    # No extra initialization needed.
    pass


func apply_state(node: Node) -> void:
    var state: Dictionary = {
        "time": time
    }

    # Assume the game world script contains the apply_state function
    node.apply_state(state)
