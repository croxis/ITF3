extends Spatial

# Seconds past jan 1st 2258
var time : float = 0
# Every minute we sync clocks
var sync_timer: float = 0


# Called when the node enters the scene tree for the first time.
func _ready():
    name = 'game'
    var charscene: PackedScene = load("res://shared/ships/earth_alliance/aurora_starfury/net_starfury.tscn")
    hashcache.star_fury = charscene.resource_path.hash()
    print("HashCache StarFury: ", hashcache.star_fury)
    loader.network.snapshot_data.register_spawner(SnapShip, hashcache.star_fury, NetDefaultSpawner.new(charscene), $ships)
    
    var hypescene: PackedScene = load("res://shared/ships/earth_alliance/hyperion/net_hyperion.tscn")
    hashcache.hyperion = hypescene.resource_path.hash()
    loader.network.snapshot_data.register_spawner(SnapShip, hashcache.hyperion, NetDefaultSpawner.new(hypescene), $ships)
    
    var omegascene: PackedScene = load("res://shared/ships/earth_alliance/omega/Omega-MattTarling.tscn")
    hashcache.omega = omegascene.resource_path.hash()
    loader.network.snapshot_data.register_spawner(SnapShip, hashcache.omega, NetDefaultSpawner.new(omegascene), $ships)
    
    var bolt_scene = preload("res://shared/scenes/bolt.tscn")
    hashcache.bolt = bolt_scene.resource_path.hash()
    # The final "self" in the spawner is where the Snapbolt will attach itself to
    loader.network.snapshot_data.register_spawner(SnapBolt, hashcache.bolt, NetDefaultSpawner.new(bolt_scene), $bolts)
    loader.network.snapshot_data.add_pre_spawned_node(SnapEnvironment, 1, self)
    
    


func _physics_process(delta):
    time += delta
    if (loader.network.has_authority()):
        sync_timer += delta
        if (sync_timer >= 60):
            sync_timer -= 60
            var snapshot: SnapEnvironment = SnapEnvironment.new(1, 2)
            snapshot.time = time
            loader.network.snapshot_entity(snapshot)


func apply_state(state: Dictionary) -> void:
    time = state.time


func get_spawn_position(index: int) -> Vector3:
    # Just some boundary check. If there is any intention to change the number of spawn points during development,
    # perhaps it would be a better idea to put those nodes into a group and use the size within this check
    if (index < 0 || index > 6):
        return Vector3(7000, 0, 0)
    
    # Locate the spawn point node given its index
    var p: Position3D = get_node("babylon_5/rotation/NetMeshCobra/spawn_points/" + str(index))
    # Provide this node's global position
    return p.global_transform.origin
