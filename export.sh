godot --export "Windows Desktop" export/itf.exe
godot --export "Linux X11" export/itf.x86_64
godot --export "Windows Desktop server" export/server.exe
godot --export "Linux X11 server" export/server.x86_64
godot --export "Resources" export/data.pck
cd export
zip itf.zip *
